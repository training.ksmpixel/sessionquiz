package com.example.admin.sessionquiz.Fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.admin.sessionquiz.Activity.SinglePlayerActivity;
import com.example.admin.sessionquiz.Adapter.SingleLevelAdapter;
import com.example.admin.sessionquiz.R;
import com.example.admin.sessionquiz.PojoClass.SingleLevelPojo;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SingleLevelFragment extends Fragment implements SingleLevelAdapter.SingleLevelAdapterListener {

    View SingleLevelView;
    private RecyclerView recyclerView;
    private List<SingleLevelPojo> singleLevelPojoList;
    private SingleLevelAdapter mAdapter;
    Context mContext;



    public SingleLevelFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        SingleLevelView = inflater.inflate(R.layout.fragment_single_level, container, false);


        mContext =getActivity();

        recyclerView = SingleLevelView.findViewById(R.id.single_recycler_view);

        singleLevelPojoList = new ArrayList<>();
        mAdapter = new SingleLevelAdapter(mContext, singleLevelPojoList, this);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext,4);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setAdapter(mAdapter);

        FetchLevelList();


        return  SingleLevelView;

    }

    public void FetchLevelList(){

        SingleLevelPojo singleLevelPojo = new SingleLevelPojo("", "1", "unlock");
        singleLevelPojoList.add(singleLevelPojo);


        singleLevelPojo = new SingleLevelPojo("", "2", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "3", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "4", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "5", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "6", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "7", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "8", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "9", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "10", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "11", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "12", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "13", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "14", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "15", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "16", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "17", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "18", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "19", "locked");
        singleLevelPojoList.add(singleLevelPojo);

        singleLevelPojo = new SingleLevelPojo("", "20", "locked");
        singleLevelPojoList.add(singleLevelPojo);

    }

    @Override
    public void onLevelItemSelected(SingleLevelPojo singleLevelPojo) {

        if(singleLevelPojo.getLocked().equals("unlock")) {
            Intent newsBriefIntent = new Intent(mContext, SinglePlayerActivity.class);
            getContext().startActivity(newsBriefIntent);
            ((Activity) mContext).finish();
        }else{
            Toast.makeText(mContext,"LEVEL LOCKED",Toast.LENGTH_LONG).show();
        }
    }

}
