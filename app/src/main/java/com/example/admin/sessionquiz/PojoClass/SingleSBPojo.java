package com.example.admin.sessionquiz.PojoClass;

public class SingleSBPojo {

    String level;
    String player;
    String score;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public SingleSBPojo() {

    }

    public SingleSBPojo(String level, String player, String score) {
        this.level = level;
        this.player = player;
        this.score = score;
    }
}
