package com.example.admin.sessionquiz.PojoClass;

public class GameReqPojo {

    String from;
    String game_id;
    String to;
    String type;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getGame_id() {
        return game_id;
    }

    public void setGame_id(String game_id) {
        this.game_id = game_id;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public GameReqPojo(String from, String game_id, String to, String type) {
        this.from = from;
        this.game_id = game_id;
        this.to = to;
        this.type = type;
    }


    public GameReqPojo() {
    }

}
