package com.example.admin.sessionquiz.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.sessionquiz.PojoClass.GameReqPojo;
import com.example.admin.sessionquiz.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class GameReqAdapter extends RecyclerView.Adapter<GameReqAdapter.ViewHolder> {
    private Context mContext;
    private List<GameReqPojo> gameReqPojoList;
    DatabaseReference gameReqDB;

    public GameReqAdapter(Context mContext, List<GameReqPojo> gameReqPojoList) {
        this.mContext = mContext;
        this.gameReqPojoList = gameReqPojoList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.game_request, parent, false);

        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        GameReqPojo gameReqPojo = gameReqPojoList.get(position);
        String from = gameReqPojo.getFrom();
        final String to = gameReqPojo.getTo();
        final String game_id = gameReqPojo.getGame_id();
        gameReqDB = FirebaseDatabase.getInstance().getReference().child("Users").child(from);
        gameReqDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final String reqPlayer = dataSnapshot.child("user_name").getValue().toString();
                holder.game_invite.setText(reqPlayer);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        holder.inviteAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext,"Clicked",Toast.LENGTH_LONG).show();
                DatabaseReference Game_Creation = FirebaseDatabase.getInstance().getReference("Game_Accepted").child(String.valueOf(game_id)).child(to);
                Game_Creation.setValue(to);

            }
        });

        holder.inviteDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return gameReqPojoList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView img_req;
        TextView game_invite;
        Button inviteAccept,inviteDecline;

        public ViewHolder(View itemView) {
            super(itemView);

            img_req = itemView.findViewById(R.id.img_req);
            game_invite = itemView.findViewById(R.id.txt_invite);
            inviteAccept = itemView.findViewById(R.id.req_accept);
            inviteDecline = itemView.findViewById(R.id.req_decline);

        }
    }
}
