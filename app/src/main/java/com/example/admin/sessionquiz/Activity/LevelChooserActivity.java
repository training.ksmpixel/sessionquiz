package com.example.admin.sessionquiz.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.FrameLayout;


import com.example.admin.sessionquiz.R;
import com.example.admin.sessionquiz.Fragment.SingleLevelFragment;

import java.util.Random;

public class LevelChooserActivity extends AppCompatActivity {

    String keyFragment;
    FrameLayout mainActivity2Layout;
    private android.support.v4.app.FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_chooser);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            keyFragment = extras.getString("key");
        }

        mainActivity2Layout =findViewById(R.id.main_content2);

        switch (keyFragment) {
            case "single":
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_content2, new SingleLevelFragment());
                fragmentTransaction.commit();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;

        }
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LevelChooserActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
}
