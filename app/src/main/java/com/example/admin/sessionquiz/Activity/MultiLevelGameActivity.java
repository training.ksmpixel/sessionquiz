package com.example.admin.sessionquiz.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.sessionquiz.Adapter.OnlineUserInviteAdapter;
import com.example.admin.sessionquiz.PojoClass.onlineUserInvitePojo;
import com.example.admin.sessionquiz.R;
import com.example.admin.sessionquiz.otherUtils.StaticVariables;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MultiLevelGameActivity extends AppCompatActivity implements View.OnClickListener {


    private RecyclerView recyclerView;
    private List<onlineUserInvitePojo> onlineUserInvitePojoList ;
    private OnlineUserInviteAdapter onlineUserInviteAdapter;

    RelativeLayout relativeLayoutRV1;

    ImageView game_req;

    Button user_invite_done;

    TextView timerCount,timer,notifyCount;

    StaticVariables staticVariables = new StaticVariables();
    SharedPreferences sharedPreferences;
    String PlayerName;

    Animation myAnim;
    ProgressBar progressBar;
    AlertDialog.Builder dialog;
    AlertDialog alertDialog;
    Handler handler = new Handler();
    int n;

    CountDownTimer waitTimer;

    FirebaseUser current_player;
    String player_uid;

    //DatabaseRefernce
    DatabaseReference FirebaseTimer;
    DatabaseReference reference;


    //ValueEventListener

    ValueEventListener GameCreateListener;
    ValueEventListener GameTimerListener;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_level_game);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //Firebase CurrentUser
        current_player = FirebaseAuth.getInstance().getCurrentUser();
        player_uid=current_player.getUid();

        sharedPreferences = getSharedPreferences(staticVariables.LOG_PREF, Context.MODE_PRIVATE);
        PlayerName = sharedPreferences.getString(staticVariables.PLAYER_NAME,"NULL");

        //Database Refernce initialize

        FirebaseTimer = FirebaseDatabase.getInstance().getReference("Game_Created").child(player_uid).child("game_timer");
        reference = FirebaseDatabase.getInstance().getReference("Game_Created");

        Random rand = new Random();
        n = rand.nextInt(5) + 1;
        current_player = FirebaseAuth.getInstance().getCurrentUser();
        player_uid=current_player.getUid();
        checkCreator();
        progressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.mul_recycler_view);
        user_invite_done = findViewById(R.id.user_invite_done);
        relativeLayoutRV1 = findViewById(R.id.rv1);

        myAnim = AnimationUtils.loadAnimation(MultiLevelGameActivity.this, R.anim.bg_button_bounce);
        user_invite_done.startAnimation(myAnim);

        timerCount = findViewById(R.id.timerCount);
        timer = findViewById(R.id.timer);
        notifyCount = findViewById(R.id.notifyCount);


        notifyCount();


        game_req = findViewById(R.id.game_req);
        onlineUserInvitePojoList = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);// new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);
        onlineUserInviteAdapter =  new OnlineUserInviteAdapter(this, onlineUserInvitePojoList,n);
        recyclerView.setAdapter(onlineUserInviteAdapter);
        game_req.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MultiLevelGameActivity.this, NotificationActivity.class);
                startActivity(intent);
            }
        });

        FetchGamePlayers();
        user_invite_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             timer();
            }
        });

        reference.addValueEventListener(GameCreateListener);
       // FirebaseTimer.addValueEventListener(GameTimerListener);




        GameTimerListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()!= null) {
                    String TimerValue = dataSnapshot.getValue().toString();
                    timer.setText(TimerValue);
                    if (TimerValue.equals("done!")) {
                        FirebaseTimer.removeEventListener(GameTimerListener);

                        Intent MainGameIntent = new Intent(MultiLevelGameActivity.this, MultiPlayerActivity.class);
                        MainGameIntent.putExtra("game_creator", player_uid);
                        MainGameIntent.putExtra("game_id", n);
                        MainGameIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(MainGameIntent);
                        finish();
                    }
                }/*else {
                                Toast.makeText(MultiLevelGameActivity.this,"Game Ended Successfully",Toast.LENGTH_SHORT).show();
                            }*/


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
    }

    private void notifyCount() {
        DatabaseReference getNotifyCount = FirebaseDatabase.getInstance().getReference("Game_Notification").child(player_uid);
        getNotifyCount.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String notifyCountint = String.valueOf(dataSnapshot.getChildrenCount());
                notifyCount.setText(notifyCountint);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void checkCreator() {

        GameCreateListener =new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child(player_uid).exists()){
                    user_invite_done.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    relativeLayoutRV1.setVisibility(View.GONE);
                    timerCount.setText("Please Wait,Time Remaining");
                    timer.setVisibility(View.VISIBLE);
                    reference.removeEventListener(GameCreateListener);
                    FirebaseTimer.addValueEventListener(GameTimerListener);

                    }else {
                    user_invite_done.setVisibility(View.VISIBLE);
                    relativeLayoutRV1.setVisibility(View.VISIBLE);
                    timerCount.setText("Invite Game Players");
                    recyclerView.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
    }

    public void FetchGamePlayers(){

        FirebaseUser current_player = FirebaseAuth.getInstance().getCurrentUser();
        String player_uid=current_player.getUid();
        final DatabaseReference GamePlayerDB= FirebaseDatabase.getInstance().getReference("Users");
        onlineUserInvitePojoList.clear();
        GamePlayerDB.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                onlineUserInvitePojo onlineUserInvitePojo = dataSnapshot.getValue(onlineUserInvitePojo.class);
                onlineUserInvitePojoList.add(onlineUserInvitePojo);
                onlineUserInviteAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                FetchGamePlayers();
                onlineUserInviteAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                FetchGamePlayers();
                onlineUserInviteAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

        }

    }

    @Override
    public void onBackPressed() {
        Intent LevelChooserIntent = new Intent(MultiLevelGameActivity.this, MainActivity.class);
        LevelChooserIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        LevelChooserIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        stopService(LevelChooserIntent);
        startActivity(LevelChooserIntent);
        stopService(LevelChooserIntent);
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }



    public void timer(){
        FirebaseUser current_player = FirebaseAuth.getInstance().getCurrentUser();
        final String player_uid=current_player.getUid();

        final DatabaseReference Game_Creation = FirebaseDatabase.getInstance().getReference("Game_Created").child(player_uid).child("game_timer");

        final DatabaseReference Game_Creator = FirebaseDatabase.getInstance().getReference("Game_Accepted").child(player_uid);
        Game_Creator.child("Quiz_Ques_No").setValue(1);
        Game_Creator.child("gamePlayers").child(player_uid).child("name").setValue(PlayerName);
        Game_Creator.child("gamePlayers").child(player_uid).child("uid").setValue(player_uid);
        Game_Creator.child("gamePlayers").child(player_uid).child("score").setValue(0);



        waitTimer = new CountDownTimer(60000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {

                String TimerFirebase = ""+String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                timer.setText(""+String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

                Game_Creation.setValue(TimerFirebase);

            }

            public void onFinish() {
                timer.setText("done!");
                Game_Creation.setValue("done!");
                if(waitTimer != null) {
                    waitTimer.cancel();
                    waitTimer = null;
                }
                user_invite_done.setVisibility(View.VISIBLE);

            }
        }.start();

    }
}
