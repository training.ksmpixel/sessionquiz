package com.example.admin.sessionquiz.otherUtils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.example.admin.sessionquiz.Activity.NotificationActivity;
import com.example.admin.sessionquiz.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class messagingservice extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_fcm_nfy_1);

        final int icon = R.mipmap.ic_launcher;
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent intent = new Intent(this, NotificationActivity.class);
        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pi = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder ncb =new NotificationCompat.Builder(this);
        ncb.setContentTitle("Session Quiz");
        ncb.setContentText("Game Invite");

        // ncb.setContentText(remoteMessage.getNotification().getBody());
        ncb.setAutoCancel(true);
        ncb.setLargeIcon(largeIcon);
        ncb.setSmallIcon(R.drawable.ic_fcm_nfy_1);
        ncb.setLargeIcon(BitmapFactory.decodeResource(getResources(), icon));
        ncb.setSound(uri);
        ncb.setContentIntent(pi);

        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(0,ncb.build());
    }
}
