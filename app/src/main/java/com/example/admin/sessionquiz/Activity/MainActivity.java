package com.example.admin.sessionquiz.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.sessionquiz.Adapter.MultiSBAdapter;
import com.example.admin.sessionquiz.PojoClass.MultiSBPojo;
import com.example.admin.sessionquiz.R;
import com.example.admin.sessionquiz.Adapter.SingleSBAdapter;
import com.example.admin.sessionquiz.PojoClass.SingleSBPojo;
import com.example.admin.sessionquiz.otherUtils.StaticVariables;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView profile,singleplayer,multiplayer,gameexit,inviteApp;
    Button starGame;

    Animation animTranslate;
    Animation animRotate;
    private FirebaseAuth vAuth;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    Dialog userDialog;

    DatabaseReference firebaseDatabase;
    DatabaseReference userDatabase;

    String name;
    String player_name ;

    List<SingleSBPojo> singleSBPojoList;
    List<MultiSBPojo> multiSBPojoList;
    SingleSBAdapter singleSBAdapter;
    MultiSBAdapter multiSBAdapter;

    StaticVariables staticVariables =new StaticVariables();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        String deviceToken = FirebaseInstanceId.getInstance().getToken();

        Log.e("deviceToken ",""+deviceToken);

        sharedPreferences = getSharedPreferences(staticVariables.LOG_PREF, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        vAuth = FirebaseAuth.getInstance();
        userDatabase = FirebaseDatabase.getInstance().getReference().child("Users");

        player_name = sharedPreferences.getString(staticVariables.PLAYER_NAME,"NULL");
        Log.e("player_name",""+player_name);

        if(player_name.equals("NULL")){
            signUpDialog();
        }
        animRotate = AnimationUtils.loadAnimation(this, R.anim.anim_rotate);

        singleSBPojoList= new ArrayList<>();
        singleSBAdapter = new SingleSBAdapter(this,singleSBPojoList);

        multiSBPojoList = new ArrayList<>();
        multiSBAdapter = new MultiSBAdapter(this,multiSBPojoList);



        profile = (ImageView)findViewById(R.id.user_profile);
        singleplayer = (ImageView)findViewById(R.id.single_playSB);
        multiplayer = (ImageView)findViewById(R.id.multiplaySB);
        gameexit  = (ImageView)findViewById(R.id.exit);
        inviteApp = findViewById(R.id.inviteApp);

        starGame = findViewById(R.id.startGame);
        animTranslate = AnimationUtils.loadAnimation(this, R.anim.anim_translate);

        starGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gameStart(view);
            }
        });

        profile.setOnClickListener(this);
        singleplayer.setOnClickListener(this);
        multiplayer.setOnClickListener(this);
        gameexit.setOnClickListener(this);
        inviteApp.setOnClickListener(this);

    }

    private void signUpDialog() {
        userDialog = new Dialog(this, android.R.style.Theme_NoTitleBar_Fullscreen);
        userDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        userDialog.setCancelable(false);
        userDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        userDialog.setContentView(R.layout.dlg_user_register);


        final EditText user_name = userDialog.findViewById(R.id.edt_name);
        final Button btnselect = userDialog.findViewById(R.id.btn_submit);
        btnselect.setEnabled(false);

        user_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                name = user_name.getText().toString();
                if (name.equals("")) {
                    btnselect.setEnabled(false);
                } else {
                    btnselect.setEnabled(true);
                }

            }
        });

        btnselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                vAuth.createUserWithEmailAndPassword(name+"@brainWringer.Com","password").addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            userDialog.dismiss();
                            FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
                            String uid = current_user.getUid();

                            firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);
                            String deviceToken = FirebaseInstanceId.getInstance().getToken();


                            Map map = new HashMap();
                            map.put("user_name", name);
                            map.put("device_token", deviceToken);
                            map.put("level", "0");
                            map.put("uid", uid);

                            editor.putString(staticVariables.PLAYER_NAME,name);
                            editor.putString(staticVariables.PLAYER_LEVEL,"0");
                            editor.putString(staticVariables.FCM_TOKEN,deviceToken);
                            editor.commit();

                            firebaseDatabase.setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    }});
                            } else {

                            LayoutInflater inflater = getLayoutInflater();
                            View layout = inflater.inflate(R.layout.custom_toast,(ViewGroup) findViewById(R.id.custom_toast_container));

                            TextView text = (TextView) layout.findViewById(R.id.text);
                            text.setText("User Name Already Exists. Try another name");

                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                            toast.setDuration(Toast.LENGTH_SHORT);
                            toast.setView(layout);
                            toast.show();
                            }
                    }
                });
            }
        });

      userDialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.user_profile:

                FirebaseUser current_playe = FirebaseAuth.getInstance().getCurrentUser();
                String playe_uid=current_playe.getUid();

                final Dialog userProfileDialog=new Dialog(MainActivity.this,android.R.style.Theme_NoTitleBar_Fullscreen);
                userProfileDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
                userProfileDialog.setContentView(R.layout.dlg_user_profile);
                userProfileDialog.setCancelable(true);
                userProfileDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                final TextView playerName = userProfileDialog.findViewById(R.id.player_name);
                TextView playerScore = userProfileDialog.findViewById(R.id.player_score);
                final TextView playerLevel = userProfileDialog.findViewById(R.id.player_level);

                DatabaseReference userDB = FirebaseDatabase.getInstance().getReference("Users").child(playe_uid);
                userDB.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final String name = dataSnapshot.child("user_name").getValue().toString();
                        final String level = dataSnapshot.child("level").getValue().toString();
                        playerName.setText(name);
                        playerLevel.setText(level);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

                userProfileDialog.show();

                break;

            case R.id.single_playSB:
                final Dialog singlePlayerScoreDialog=new Dialog(MainActivity.this,android.R.style.Theme_NoTitleBar_Fullscreen);
                singlePlayerScoreDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
                singlePlayerScoreDialog.setContentView(R.layout.dlg_single_player_score_board);

                RecyclerView singleScoreBoard = singlePlayerScoreDialog.findViewById(R.id.singleScoreBoard);
                singleScoreBoard.setHasFixedSize(true);
                singleScoreBoard.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false));
                singleScoreBoard.setAdapter(singleSBAdapter);

                FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
                String uid=current_user.getUid();
                final DatabaseReference SingleSBdb= FirebaseDatabase.getInstance().getReference("SingleUserSB").child(uid);
                singleSBPojoList.clear();
                SingleSBdb.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        SingleSBPojo singleSBPojo = dataSnapshot.getValue(SingleSBPojo.class);
                        singleSBPojoList.add(singleSBPojo);
                        singleSBAdapter.notifyDataSetChanged();


                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                singlePlayerScoreDialog.show();
                break;

            case R.id.multiplaySB:
                final Dialog multiPlayerScoreDialog=new Dialog(MainActivity.this,android.R.style.Theme_NoTitleBar_Fullscreen);
                multiPlayerScoreDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
                multiPlayerScoreDialog.setContentView(R.layout.dialog_multi_player_score_board);

                RecyclerView multiScoreBoard = multiPlayerScoreDialog.findViewById(R.id.multiScoreBoard);
                multiScoreBoard.setHasFixedSize(true);
                LinearLayoutManager mLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
                multiScoreBoard.setLayoutManager(mLayoutManager);
                multiScoreBoard.setAdapter(multiSBAdapter);

                final DatabaseReference MultiSBdb= FirebaseDatabase.getInstance().getReference("MultiUserSB");
                multiSBPojoList.clear();

                Query orderByChildQuerry = MultiSBdb.orderByChild("score");

                orderByChildQuerry.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        for(DataSnapshot ds : dataSnapshot.getChildren()) {
                            String level =  ds.child("level").getValue(String.class);
                            String player =  ds.child("player").getValue(String.class);
                            String score =  ds.child("score").getValue(String.class);

                            multiSBPojoList.add(0,new MultiSBPojo(level,player,score));
                            multiSBAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                multiPlayerScoreDialog.show();
                break;

            case R.id.inviteApp:

                final DatabaseReference linkFetch =  FirebaseDatabase.getInstance().getReference("AppInviteLink");

                ValueEventListener linkFetchListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        String Link = dataSnapshot.child("link").getValue().toString();
                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hiii.. This is Nice App. Click below link to install this App "+Link);
                        startActivity(Intent.createChooser(sharingIntent, "Invite Friends Via..."));



                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                };
                linkFetch.addListenerForSingleValueEvent(linkFetchListener);



                break;
            case R.id.exit:
                onBackPressed();
                break;
        }
    }

    public void gameStart(View view) {
       /* starGame.playSoundEffect(0);
        view.startAnimation(animTranslate);*/
        int SPLASH_TIME_OUT = 300;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                final Dialog GameOptionDialog = new Dialog(MainActivity.this);
                GameOptionDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
                GameOptionDialog.setContentView(R.layout.dialog_game_player_chooser);

                final Animation animTranslate = AnimationUtils.loadAnimation(MainActivity.this, R.anim.anim_translate);

                Button singlePlayerButton = GameOptionDialog.findViewById(R.id.single_player);
                Button multiPlayerButton = GameOptionDialog.findViewById(R.id.multi_player);

                singlePlayerButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        view.startAnimation(animTranslate);
                        int TIME_OUT = 1000;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                GameOptionDialog.dismiss();
                                Intent singleIntent = new Intent(MainActivity.this, LevelChooserActivity.class);
                                singleIntent.putExtra("key","single");
                                singleIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(singleIntent);
                                finish();
                                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                            }
                        }, TIME_OUT);
                    }
                });

                multiPlayerButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        starGame.setAnimation(null);
                        view.startAnimation(animTranslate);
                        int TIME_OUT = 1000;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                GameOptionDialog.dismiss();
                                Intent multiIntent = new Intent(MainActivity.this, MultiLevelGameActivity.class);
                                multiIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(multiIntent);
                                finish();
                                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                            }
                        }, TIME_OUT);
                    }
                });

                GameOptionDialog.show();
            }
        }, SPLASH_TIME_OUT);

    }


    @Override
    public void onBackPressed() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_app_exit, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);

        ImageView yesIV = dialogView.findViewById(R.id.y);
        ImageView noIV = dialogView.findViewById(R.id.n);

        yesIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
        noIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();
        }
}
