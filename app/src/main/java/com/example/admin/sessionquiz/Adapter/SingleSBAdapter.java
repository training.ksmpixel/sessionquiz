package com.example.admin.sessionquiz.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.example.admin.sessionquiz.Animation.AnimationUtil;
import com.example.admin.sessionquiz.R;
import com.example.admin.sessionquiz.PojoClass.SingleSBPojo;

import java.util.List;

public class SingleSBAdapter extends RecyclerView.Adapter<SingleSBAdapter.SingleSBViewHolder> {
    private Context context;
    private List<SingleSBPojo> singleSBPojoList;
    private int previousPosition = 0;



    public SingleSBAdapter(Context context, List<SingleSBPojo> singleSBPojoList) {

        this.singleSBPojoList = singleSBPojoList;
        this.context=context;

    }

    @NonNull
    @Override
    public SingleSBViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.single_item_ssb_layout, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        return new SingleSBViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SingleSBViewHolder holder, int position) {
        SingleSBPojo singleSBPojo = singleSBPojoList.get(position);

        holder.t1.setText(singleSBPojo.getLevel());
        holder.t2.setText(singleSBPojo.getPlayer());
        holder.t3.setText(singleSBPojo.getScore());


        if(position > previousPosition) {
            // We are scrolling DOWN
            AnimationUtil.animateUpDown(holder, true);
        } else {
            // We are scrolling UP
            AnimationUtil.animateUpDown(holder, false);
        }

        previousPosition = position;


    }

    @Override
    public int getItemCount() {
        return singleSBPojoList.size();
    }

    public class SingleSBViewHolder extends RecyclerView.ViewHolder{

       TextView t1,t2,t3,t4;
       
        public SingleSBViewHolder(View itemView) {
            super(itemView);
            t1 = itemView.findViewById(R.id.s_no);
            t2 = itemView.findViewById(R.id.s_user);
            t3 = itemView.findViewById(R.id.s_points);
            //t4 = itemView.findViewById(R.id.s_date);

        }
    }


}
