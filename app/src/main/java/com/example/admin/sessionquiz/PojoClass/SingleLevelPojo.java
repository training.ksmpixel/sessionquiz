package com.example.admin.sessionquiz.PojoClass;

public class SingleLevelPojo {

    String level;
    String level_id;
    String locked;

    public SingleLevelPojo() {
    }

    public SingleLevelPojo(String level, String level_id, String locked) {
        this.level = level;
        this.level_id = level_id;
        this.locked = locked;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getLevel_id() {
        return level_id;
    }

    public void setLevel_id(String level_id) {
        this.level_id = level_id;
    }

    public String getLocked() {
        return locked;
    }

    public void setLocked(String locked) {
        this.locked = locked;
    }
}
