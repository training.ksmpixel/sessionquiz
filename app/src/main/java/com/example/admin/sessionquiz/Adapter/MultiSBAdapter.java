package com.example.admin.sessionquiz.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.sessionquiz.Animation.AnimationUtil;
import com.example.admin.sessionquiz.PojoClass.MultiSBPojo;
import com.example.admin.sessionquiz.R;

import java.util.List;

public class MultiSBAdapter extends RecyclerView.Adapter<MultiSBAdapter.multiSBViewHolder> {
    private Context context;
    private List<MultiSBPojo> multiSBPojoList;
    private int previousPosition = 0;


    public MultiSBAdapter(Context context, List<MultiSBPojo> multiSBPojoList) {
        this.context = context;
        this.multiSBPojoList = multiSBPojoList;
    }



    @NonNull
    @Override
    public multiSBViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.multi_item_sb_layout, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        return new multiSBViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull multiSBViewHolder holder, int position) {
        MultiSBPojo multiSBPojo = multiSBPojoList.get(position);


        String PlayerRank = String.valueOf(position+1);

        holder.id.setText(multiSBPojo.getLevel());
        holder.name.setText(multiSBPojo.getPlayer());
        holder.score.setText(multiSBPojo.getScore());

        if(position > previousPosition) {
            // We are scrolling DOWN
            AnimationUtil.animateUpDown(holder, true);
        } else {
            // We are scrolling UP
            AnimationUtil.animateUpDown(holder, false);
        }

        previousPosition = position;


    }


    @Override
    public int getItemCount() {
        return multiSBPojoList.size();
    }



    public class multiSBViewHolder extends RecyclerView.ViewHolder{

        TextView id,name,score;

        public multiSBViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.game_id);
            name = itemView.findViewById(R.id.game_user);
            score= itemView.findViewById(R.id.user_score);
        }
    }
}
