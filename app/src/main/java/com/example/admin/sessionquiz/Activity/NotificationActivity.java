package com.example.admin.sessionquiz.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.admin.sessionquiz.Adapter.GameReqAdapter;
import com.example.admin.sessionquiz.Adapter.Mul_Game_Req_Adapter;
import com.example.admin.sessionquiz.PojoClass.GameReqPojo;
import com.example.admin.sessionquiz.PojoClass.SingleSBPojo;
import com.example.admin.sessionquiz.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity {
    List<GameReqPojo> gameReqPojoList;
    Context mContext;
    Mul_Game_Req_Adapter reqAdapter;
    RecyclerView recyclerView;
    int n;
    RelativeLayout rootRV;
    TextView timerNA,notifyHeading;
    Handler handler = new Handler();
    FirebaseUser current_player;
    String player_uid;

    //database reference
    DatabaseReference NotifyAcceptPlay;
    DatabaseReference GameReq;
    DatabaseReference NotifyAcceptPlayNotify;

    //Value Listener
    ValueEventListener NotifyAcceptListener;
    ChildEventListener GameReqListener;
    ValueEventListener ExistNotifyAcceptListener;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        current_player = FirebaseAuth.getInstance().getCurrentUser();
        player_uid=current_player.getUid();

        //database reference intialization
        NotifyAcceptPlay = FirebaseDatabase.getInstance().getReference("Notification_Accepted");
        GameReq= FirebaseDatabase.getInstance().getReference("Game_Notification").child(player_uid);
        NotifyAcceptPlayNotify = FirebaseDatabase.getInstance().getReference("Notification_Accepted").child(player_uid);


        recyclerView = (RecyclerView)findViewById(R.id.notify_recycler_view);
        rootRV = findViewById(R.id.rootRV);
        timerNA = findViewById(R.id.timerNA);
        notifyHeading = findViewById(R.id.notifyHeading);

        gameReqPojoList = new ArrayList<>();

        CheckGamePlayer();
        reqAdapter = new Mul_Game_Req_Adapter(this, gameReqPojoList, n);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);;
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(reqAdapter);
        getReqList();

    }

    private void CheckGamePlayer() {
        NotifyAcceptListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child(player_uid).exists()){
                    recyclerView.setVisibility(View.GONE);
                    notifyHeading.setText("Please Wait,Time Remaining");
                    rootRV.setVisibility(View.VISIBLE);
                    NotifyAcceptPlay.removeEventListener(NotifyAcceptListener);
                    ExistNotifyAcceptListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.getValue()!= null) {
                                String gameCreator = dataSnapshot.child("creator_id").getValue().toString();
                                String game_id = dataSnapshot.child("game_id").getValue().toString();
                                String game_timer = dataSnapshot.child("game_timer").getValue().toString();
                                timerNA.setText(game_timer);
                                int id_game = Integer.parseInt(game_id);
                                if (game_timer.equals("done!")) {
                                    NotifyAcceptPlayNotify.removeEventListener(ExistNotifyAcceptListener);
                                    Intent MainGameIntent = new Intent(NotificationActivity.this, MultiPlayerActivity.class);
                                    MainGameIntent.putExtra("game_creator", gameCreator);
                                    MainGameIntent.putExtra("game_id", id_game);
                                    MainGameIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(MainGameIntent);
                                    finish();
                                }
                            }else{
                                }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                    };

                    NotifyAcceptPlayNotify.addValueEventListener(ExistNotifyAcceptListener);

                }else {
                    notifyHeading.setText("GAME INVITE REQUESTS");
                    rootRV.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                }
        };
        NotifyAcceptPlay.addValueEventListener(NotifyAcceptListener);
        }
        public void getReqList(){
        gameReqPojoList.clear();
        GameReqListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                GameReqPojo gameReqPojo = dataSnapshot.getValue(GameReqPojo.class);
                gameReqPojoList.add(gameReqPojo);
                reqAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                getReqList();
                reqAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                getReqList();
                reqAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

            GameReq.addChildEventListener(GameReqListener);
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(NotificationActivity.this, MultiLevelGameActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
}
