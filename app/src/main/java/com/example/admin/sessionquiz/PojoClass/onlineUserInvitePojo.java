package com.example.admin.sessionquiz.PojoClass;

public class onlineUserInvitePojo {

    String device_token;
    String level;
    String user_name;
    String uid;

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public onlineUserInvitePojo(String device_token, String level, String user_name, String uid) {
        this.device_token = device_token;
        this.level = level;
        this.user_name = user_name;
        this.uid = uid;
    }

    public onlineUserInvitePojo() {
    }
}
