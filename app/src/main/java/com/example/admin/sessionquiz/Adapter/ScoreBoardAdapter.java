package com.example.admin.sessionquiz.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.sessionquiz.PojoClass.MultiScorePojo;
import com.example.admin.sessionquiz.R;

import java.util.List;

public class ScoreBoardAdapter extends RecyclerView.Adapter<ScoreBoardAdapter.ViewHolder> {
    private Context context;
    private List<MultiScorePojo> multiScorePojoList;
    String score;
    String pos;

    public ScoreBoardAdapter(Context context, List<MultiScorePojo> multiScorePojoList) {
        this.context = context;
        this.multiScorePojoList = multiScorePojoList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.mul__player_score_update, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MultiScorePojo multiScorePojo = multiScorePojoList.get(position);
        score = String.valueOf(multiScorePojo.getScore());
        pos = String.valueOf(position+1);

        holder.player_Name.setText(multiScorePojo.getName());
        holder.player_Score.setText(score);
        holder.player_Id.setText(pos);

    }

    @Override
    public int getItemCount() {
        return multiScorePojoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView player_Name,player_Score,player_Id;

        public ViewHolder(View itemView) {
            super(itemView);
            player_Name = itemView.findViewById(R.id.mplayer_name);
            player_Score = itemView.findViewById(R.id.mplayer_score);
            player_Id  = itemView.findViewById(R.id.mplayer_id);

        }
    }
}
