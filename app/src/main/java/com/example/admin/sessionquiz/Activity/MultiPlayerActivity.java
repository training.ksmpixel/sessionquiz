package com.example.admin.sessionquiz.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.sessionquiz.PojoClass.MultiScorePojo;
import com.example.admin.sessionquiz.R;
import com.example.admin.sessionquiz.Adapter.ScoreBoardAdapter;
import com.example.admin.sessionquiz.otherUtils.StaticVariables;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MultiPlayerActivity extends AppCompatActivity implements View.OnClickListener {
    TextView MainCounter,nextQuizCounter,next_q_timer,skip_time,playercount;

    TextView quiz_question,TV_option_a,TV_option_b,TV_option_c,TV_option_d;

    int quesNo;
    int Score = 0;

    DatabaseReference mUserRef;
    String current_user_uid;
    String User_Answer = "nothing_selected";
    String answer = "null";

    Handler handler;

    boolean isPaused =false;
    boolean isCanceled = false;

    ScoreBoardAdapter scoreBoardAdapter;
    List<MultiScorePojo> multiScorePojoList;

    StaticVariables staticVariables = new StaticVariables();
    SharedPreferences singleSP ;
    String playerName;
    String creatorKey;
    int inviteKey;
    DatabaseReference Game_Ques_No;
    DatabaseReference Game_CreationUsers;
    DatabaseReference Game_Deletion;

    ValueEventListener QuestionFetchListener;

    Dialog resultDialog;
    Dialog alertDialog;
    ImageView smiley_answer;
    TextView result_text;
    String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_player);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            creatorKey = bundle.getString("game_creator");
            inviteKey = bundle.getInt("game_id");
        }


        singleSP = getSharedPreferences(staticVariables.LOG_PREF, Context.MODE_PRIVATE);
        playerName = singleSP.getString(staticVariables.PLAYER_NAME,"NULL");

        FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
        current_user_uid = current_user.getUid();
        uid=current_user.getUid();

        handler = new Handler();

        multiScorePojoList = new ArrayList<>();
        scoreBoardAdapter = new ScoreBoardAdapter(this,multiScorePojoList);

        resultDialog = new Dialog(MultiPlayerActivity.this);
        resultDialog.setCancelable(false);
        resultDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        resultDialog.setContentView(R.layout.dialog_result_mul_player);

        MainCounter = findViewById(R.id.quiz_main_timer);
        playercount = findViewById(R.id.mp_list);
        nextQuizCounter = findViewById(R.id.next_quiz_timer);
        nextQuizCounter.setText("0");
        next_q_timer= findViewById(R.id.next_q_timer);
        skip_time = findViewById(R.id.sb_list_dialog);

        quiz_question = findViewById(R.id.quiz_question);
        TV_option_a = findViewById(R.id.option_a);
        TV_option_b = findViewById(R.id.option_b);
        TV_option_c = findViewById(R.id.option_c);
        TV_option_d = findViewById(R.id.option_d);

        Game_Ques_No = FirebaseDatabase.getInstance().getReference("Game_Accepted").child(creatorKey).child("Quiz_Ques_No");
        Game_CreationUsers = FirebaseDatabase.getInstance().getReference("Game_Accepted").child(creatorKey).child("gamePlayers").child(current_user_uid);
        Game_Deletion = FirebaseDatabase.getInstance().getReference("Game_Accepted").child(creatorKey);

        Game_CreationUsers.child("score").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    Score = Integer.parseInt(dataSnapshot.getValue().toString());
                    nextQuizCounter.setText(String.valueOf(Score));
                }else{
                    if (resultDialog != null) {
                        resultDialog.dismiss();
                        resultDialog = null;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

      QuestionFetchListener = new ValueEventListener() {
          @Override
          public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
              if (dataSnapshot.getValue() != null) {
                  quesNo = Integer.parseInt(dataSnapshot.getValue().toString());

                  handler.postDelayed(new Runnable() {
                      public void run() {
                          if (alertDialog != null) {
                              alertDialog.dismiss();
                              alertDialog = null;
                          }

                          if (quesNo <= 5) {
                              Log.e("ques no", "" + quesNo);
                              getQuiz();
                          } else {
                              quesNo = 0;
                              Log.e("s no", "" + quesNo);
                              addDBSingleScore();
                          }
                      }
                  }, 7000);
              }
              else{
                  if (resultDialog != null) {
                      resultDialog.dismiss();
                      resultDialog = null;
                  }
                  isPaused = true;
                  Intent intent = new Intent(MultiPlayerActivity.this, MultiLevelGameActivity.class);
                  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                  startActivity(intent);
                  finish();
                  overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
              }
          }

          @Override
          public void onCancelled(@NonNull DatabaseError databaseError) {

          }
      };

        Game_Ques_No.addValueEventListener(QuestionFetchListener);



        TV_option_a.setOnClickListener(this);
        TV_option_b.setOnClickListener(this);
        TV_option_c.setOnClickListener(this);
        TV_option_d.setOnClickListener(this);
    }



    public void getQuiz() {


            User_Answer = "nothing_selected";
            quiz_question.setText("");

            TV_option_a.setText("");
            TV_option_a.setBackgroundResource(R.drawable.bg_game_text_view);
            TV_option_a.setTextColor(Color.BLACK);

            TV_option_b.setText("");
            TV_option_b.setBackgroundResource(R.drawable.bg_game_text_view);
            TV_option_b.setTextColor(Color.BLACK);

            TV_option_c.setText("");
            TV_option_c.setBackgroundResource(R.drawable.bg_game_text_view);
            TV_option_c.setTextColor(Color.BLACK);

            TV_option_d.setText("");
            TV_option_d.setBackgroundResource(R.drawable.bg_game_text_view);
            TV_option_d.setTextColor(Color.BLACK);

            String game_Id = String.valueOf(inviteKey);

            mUserRef = FirebaseDatabase.getInstance().getReference("Game_ID").child(String.valueOf(game_Id)).child(String.valueOf(quesNo));

            mUserRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    answer = dataSnapshot.child("answer").getValue().toString();
                    final String option_a = dataSnapshot.child("option_a").getValue().toString();
                    final String option_b = dataSnapshot.child("option_b").getValue().toString();
                    final String option_c = dataSnapshot.child("option_c").getValue().toString();
                    final String option_d = dataSnapshot.child("option_d").getValue().toString();
                    final String Question = dataSnapshot.child("question").getValue().toString();

                    TV_option_a.setEnabled(true);
                    TV_option_b.setEnabled(true);
                    TV_option_c.setEnabled(true);
                    TV_option_d.setEnabled(true);

                    Animation bottomToTop = AnimationUtils.loadAnimation(MultiPlayerActivity.this, R.anim.anim_alpha);
                    quiz_question.startAnimation(bottomToTop);
                    quiz_question.setText(quesNo-1+" . " + Question);

                    TV_option_a.startAnimation(bottomToTop);
                    TV_option_a.setText(option_a);

                    TV_option_b.startAnimation(bottomToTop);
                    TV_option_b.setText(option_b);

                    TV_option_c.startAnimation(bottomToTop);
                    TV_option_c.setText(option_c);

                    TV_option_d.startAnimation(bottomToTop);
                    TV_option_d.setText(option_d);

                    reverseTimer(option_a, option_b, option_c, option_d);
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            quesNo++;
    }
    private void reverseTimer(final String op_a, final String op_b, final String op_c, final String op_d){

        new CountDownTimer(15000, 1000) {

            public void onTick(long millisUntilFinished) {

                if (isPaused || isCanceled) {
                    cancel();
                } else {

                    MainCounter.setText("" + millisUntilFinished / 1000);
                }
            }

            public void onFinish() {

                MainCounter.setText("0");
                final String mainCounterText = MainCounter.getText().toString();
                if (mainCounterText.equals("0")) {
                    getResult(op_a, op_b, op_c, op_d);
                }
            }
        }.start();
    }



    public void getResult(final String op_a, final String op_b, final String op_c, final String op_d) {

        if (User_Answer.equals(answer)) {

            Score = Score + 10;
            nextQuizCounter.setText(String.valueOf(Score));

            Game_CreationUsers.child("score").setValue(Score);

            alertDialog = new Dialog(MultiPlayerActivity.this);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.dialog_answer);
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            smiley_answer = alertDialog.findViewById(R.id.smiley_answer);
            result_text = alertDialog.findViewById(R.id.result_text);


            smiley_answer.setImageResource(R.drawable.ic_happy);

            Animation bottomToTop = AnimationUtils.loadAnimation(MultiPlayerActivity.this, R.anim.anim_alpha);
            result_text.startAnimation(bottomToTop);
            result_text.setText("CORRECT");
            result_text.setTextColor(Color.GREEN);

            alertDialog.show();




            if (User_Answer.equals(op_a)) {
                TV_option_a.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                TV_option_a.setTextColor(Color.WHITE);
            } else if (User_Answer.equals(op_b)) {
                TV_option_b.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                TV_option_b.setTextColor(Color.WHITE);

            } else if (User_Answer.equals(op_c)) {
                TV_option_c.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                TV_option_c.setTextColor(Color.WHITE);

            } else if (User_Answer.equals(op_d)) {
                TV_option_d.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                TV_option_d.setTextColor(Color.WHITE);
            } else {

            }
        } else {

            alertDialog = new Dialog(MultiPlayerActivity.this);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.dialog_answer);
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            smiley_answer = alertDialog.findViewById(R.id.smiley_answer);
            result_text = alertDialog.findViewById(R.id.result_text);

            smiley_answer.setImageResource(R.drawable.ic_crying);
            Animation bottomToTop = AnimationUtils.loadAnimation(MultiPlayerActivity.this, R.anim.anim_alpha);
            result_text.startAnimation(bottomToTop);
            result_text.setText("Wrong Answer");
            result_text.setTextColor(Color.RED);

            alertDialog.show();

            if (answer.equals(op_a)) {
                TV_option_a.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                TV_option_a.setTextColor(Color.WHITE);
            } else if (answer.equals(op_b)) {
                TV_option_b.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                TV_option_b.setTextColor(Color.WHITE);

            } else if (answer.equals(op_c)) {
                TV_option_c.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                TV_option_c.setTextColor(Color.WHITE);

            } else if (User_Answer.equals(op_d)) {
                TV_option_d.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                TV_option_d.setTextColor(Color.WHITE);
            } else {

            }

            if (User_Answer.equals(op_a)) {
                TV_option_a.setBackgroundResource(R.drawable.bg_game_text_view_incorrect);
                TV_option_a.setTextColor(Color.WHITE);
            } else if (User_Answer.equals(op_b)) {
                TV_option_b.setBackgroundResource(R.drawable.bg_game_text_view_incorrect);
                TV_option_b.setTextColor(Color.WHITE);

            } else if (User_Answer.equals(op_c)) {
                TV_option_c.setBackgroundResource(R.drawable.bg_game_text_view_incorrect);
                TV_option_c.setTextColor(Color.WHITE);

            } else if (User_Answer.equals(op_d)) {
                TV_option_d.setBackgroundResource(R.drawable.bg_game_text_view_incorrect);
                TV_option_d.setTextColor(Color.WHITE);
            } else {
            }

        }
        Game_Ques_No = FirebaseDatabase.getInstance().getReference("Game_Accepted").child(creatorKey).child("Quiz_Ques_No");
        Game_Ques_No.setValue(quesNo);
    }

    public void addDBSingleScore(){

        final DatabaseReference MultiSBUpdate = FirebaseDatabase.getInstance().getReference().child("MultiUserSB").child(uid);
        MultiSBUpdate.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null) {

                    String currentMaxScore = dataSnapshot.child("score").getValue().toString();

                    int cHScore = Integer.parseInt(currentMaxScore);

                    if (Score >= cHScore) {

                        HashMap<String, String> userMap = new HashMap<String, String>();
                        String randomRecord = MultiSBUpdate.push().getKey();
                        userMap.put("level", "1");
                        userMap.put("player", playerName);
                        userMap.put("score", String.valueOf(Score));

                        MultiSBUpdate.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    if (!((Activity) MultiPlayerActivity.this).isFinishing()) {
                                        getListResult();
                                    }
                                }
                            }
                        });


                    } else {
                        if (!((Activity) MultiPlayerActivity.this).isFinishing()) {
                            getListResult();
                        }
                    }

                } else {

                    HashMap<String, String> userMap = new HashMap<String, String>();

                    String randomRecord = MultiSBUpdate.push().getKey();

                    userMap.put("level", "1");
                    userMap.put("player", playerName);
                    userMap.put("score", String.valueOf(Score));

                    MultiSBUpdate.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                if (!((Activity) MultiPlayerActivity.this).isFinishing()) {
                                    getListResult();
                                }
                            }
                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




        if(creatorKey.equals(uid)) {
            DatabaseReference Game_Creation = FirebaseDatabase.getInstance().getReference("Game_Created").child(creatorKey);
            Game_Creation.removeValue();
        }else{
            DatabaseReference Notification_Accepted = FirebaseDatabase.getInstance().getReference("Notification_Accepted").child(uid);
            Notification_Accepted.removeValue();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        Game_Ques_No.removeEventListener(QuestionFetchListener);
        isPaused = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Game_Ques_No.removeEventListener(QuestionFetchListener);
        isPaused = true;
    }

    private void getListResult() {

        Game_Ques_No.removeEventListener(QuestionFetchListener);

        RecyclerView recyclerView = resultDialog.findViewById(R.id.multiScoreUpdate);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(MultiPlayerActivity.this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(scoreBoardAdapter);

        DatabaseReference scoreupdate = FirebaseDatabase.getInstance().getReference("Game_Accepted").child(creatorKey).child("gamePlayers");
        multiScorePojoList.clear();
        scoreupdate.orderByChild("score").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                MultiScorePojo multiScorePojo = dataSnapshot.getValue(MultiScorePojo.class);
                multiScorePojoList.add(multiScorePojo);
                scoreBoardAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                MultiScorePojo multiScorePojo = dataSnapshot.getValue(MultiScorePojo.class);

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                MultiScorePojo multiScorePojo = dataSnapshot.getValue(MultiScorePojo.class);

                isPaused = true;
                Intent intent = new Intent(MultiPlayerActivity.this, MultiLevelGameActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        resultDialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    resultDialog.dismiss();
                    Game_Deletion.removeValue();
                    isPaused = true;
                    Intent intent = new Intent(MultiPlayerActivity.this, MultiLevelGameActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

                }
                return true;
            }
        });
        resultDialog.show();
    }

    @Override
    public void onBackPressed() {


        final android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_app_exit, null);
        dialogBuilder.setView(dialogView);
        final android.app.AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);

        ImageView yesIV = dialogView.findViewById(R.id.y);
        ImageView noIV = dialogView.findViewById(R.id.n);

        yesIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
                Game_Ques_No.removeEventListener(QuestionFetchListener);

                if(creatorKey.equals(uid)) {
                    DatabaseReference Game_Creation = FirebaseDatabase.getInstance().getReference("Game_Created").child(creatorKey);
                    Game_Creation.removeValue();
                }else{
                    DatabaseReference Notification_Accepted = FirebaseDatabase.getInstance().getReference("Notification_Accepted").child(uid);
                    Notification_Accepted.removeValue();
                }


                isPaused = true;
                Intent intent = new Intent(MultiPlayerActivity.this, MultiLevelGameActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
        noIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.option_a:

                TV_option_a.setBackgroundResource(R.drawable.bg_game_text_view_is_selected);
                TV_option_a.setTextColor(Color.WHITE);

                User_Answer = TV_option_a.getText().toString();

                TV_option_b.setEnabled(false);
                TV_option_c.setEnabled(false);
                TV_option_d.setEnabled(false);


                break;
            case R.id.option_b:

                TV_option_b.setBackgroundResource(R.drawable.bg_game_text_view_is_selected);
                TV_option_b.setTextColor(Color.WHITE);

                User_Answer = TV_option_b.getText().toString();

                TV_option_a.setEnabled(false);
                TV_option_c.setEnabled(false);
                TV_option_d.setEnabled(false);

                break;
            case R.id.option_c:

                TV_option_c.setBackgroundResource(R.drawable.bg_game_text_view_is_selected);
                TV_option_c.setTextColor(Color.WHITE);

                User_Answer = TV_option_c.getText().toString();

                TV_option_b.setEnabled(false);
                TV_option_a.setEnabled(false);
                TV_option_d.setEnabled(false);

                break;
            case R.id.option_d:
                TV_option_d.setBackgroundResource(R.drawable.bg_game_text_view_is_selected);
                TV_option_d.setTextColor(Color.WHITE);

                User_Answer = TV_option_d.getText().toString();

                TV_option_b.setEnabled(false);
                TV_option_c.setEnabled(false);
                TV_option_a.setEnabled(false);
                break;

        }
    }
}
