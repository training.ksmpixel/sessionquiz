package com.example.admin.sessionquiz.Animation;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView;


public class AnimationUtil {
    public static void animateUpDown(RecyclerView.ViewHolder holder , boolean upDown){
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator animatorTranslateY = ObjectAnimator.ofFloat(holder.itemView, "translationY", upDown==true ? 200 : -200, 0);
        animatorTranslateY.setDuration(500);

        animatorSet.playTogether(animatorTranslateY);
        animatorSet.start();
    }

    public static void animateRightLeft(RecyclerView.ViewHolder holder, boolean rightLeft){
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator animatorTranslateX = ObjectAnimator.ofFloat(holder.itemView,"translationX",rightLeft==true ? 200 : -200, 0); //-50,50,-30,30,-20,20,-5,5,0);
        animatorTranslateX.setDuration(500);

        animatorSet.playTogether(animatorTranslateX);
        animatorSet.start();
    }


    public static void animateBoth(RecyclerView.ViewHolder holder , boolean bothDir){
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator animatorTranslateY = ObjectAnimator.ofFloat(holder.itemView, "translationY", bothDir==true ? 200 : -200, 0);
        animatorTranslateY.setDuration(500);

        ObjectAnimator animatorTranslateX = ObjectAnimator.ofFloat(holder.itemView,"translationX",-50,50,-30,30,-20,20,-5,5,0);
        animatorTranslateX.setDuration(500);

        animatorSet.playTogether(animatorTranslateX,animatorTranslateY);
        animatorSet.start();
    }
}
