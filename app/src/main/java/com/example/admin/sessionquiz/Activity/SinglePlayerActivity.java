package com.example.admin.sessionquiz.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.sessionquiz.R;
import com.example.admin.sessionquiz.otherUtils.StaticVariables;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class SinglePlayerActivity extends AppCompatActivity implements View.OnClickListener {

    TextView MainCounter,nextQuizCounter,next_q_timer,skip_time;
    MediaPlayer mp;

    TextView quiz_question,TV_option_a,TV_option_b,TV_option_c,TV_option_d;

    int i = 1;

    int Score = 0;

    private FirebaseAuth mAuth;
    private DatabaseReference mUserRef;

    String User_Answer = "nothing_selected";
    String answer = "null";

    Handler handler;

    boolean isPaused =false;
    boolean isCanceled = false;

    StaticVariables staticVariables = new StaticVariables();
    SharedPreferences singleSP ;
    String playerName;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_player);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        singleSP = getSharedPreferences(staticVariables.LOG_PREF, Context.MODE_PRIVATE);
        playerName = singleSP.getString(staticVariables.PLAYER_NAME,"NULL");


        MainCounter = findViewById(R.id.quiz_main_timer);
        nextQuizCounter = findViewById(R.id.next_quiz_timer);
        next_q_timer= findViewById(R.id.next_q_timer);
        skip_time = findViewById(R.id.sb_list_dialog);





        quiz_question = findViewById(R.id.quiz_question);
        TV_option_a = findViewById(R.id.option_a);
        TV_option_b = findViewById(R.id.option_b);
        TV_option_c = findViewById(R.id.option_c);
        TV_option_d = findViewById(R.id.option_d);



        handler = new Handler();

        getQuiz();

        TV_option_a.setOnClickListener(this);
        TV_option_b.setOnClickListener(this);
        TV_option_c.setOnClickListener(this);
        TV_option_d.setOnClickListener(this);
       // skip_time.setOnClickListener(this);



    }
/*
    @Override
    protected void onStart() {
        super.onStart();
        i--;
        getQuiz();
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        isPaused = false;
      //  i--;
        //getQuiz();

    }

    @Override
    protected void onStop() {
        super.onStop();
        isPaused = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPaused = true;
    }

    public void getQuiz() {

        MainCounter.setVisibility(View.INVISIBLE);
        handler.postDelayed(new Runnable() {
            public void run() {

                new CountDownTimer(10000, 1000) {

                    public void onTick(long millisUntilFinished) {

                        next_q_timer.setVisibility(View.VISIBLE);
                        next_q_timer.setText("" + millisUntilFinished / 1000);
                    }

                    public void onFinish() {
                        next_q_timer.setText("START");
                        final String nextQuizTimerString = next_q_timer.getText().toString();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                if (nextQuizTimerString.equals("START")) {
                                    next_q_timer.setVisibility(View.GONE);
                                    MainCounter.setVisibility(View.VISIBLE);




                                    Log.e("i ", "" + i);
        User_Answer = "nothing_selected";

        quiz_question.setText("");

        TV_option_a.setText("");
        TV_option_a.setBackgroundResource(R.drawable.bg_game_text_view);
        TV_option_a.setTextColor(Color.BLACK);

        TV_option_b.setText("");
        TV_option_b.setBackgroundResource(R.drawable.bg_game_text_view);
        TV_option_b.setTextColor(Color.BLACK);

        TV_option_c.setText("");
        TV_option_c.setBackgroundResource(R.drawable.bg_game_text_view);
        TV_option_c.setTextColor(Color.BLACK);

        TV_option_d.setText("");
        TV_option_d.setBackgroundResource(R.drawable.bg_game_text_view);
        TV_option_d.setTextColor(Color.BLACK);


        mUserRef = FirebaseDatabase.getInstance().getReference("Game_ID").child("100").child(String.valueOf(i));

        mUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                answer = dataSnapshot.child("answer").getValue().toString();
                final String option_a = dataSnapshot.child("option_a").getValue().toString();
                final String option_b = dataSnapshot.child("option_b").getValue().toString();
                final String option_c = dataSnapshot.child("option_c").getValue().toString();
                final String option_d = dataSnapshot.child("option_d").getValue().toString();
                final String Question = dataSnapshot.child("question").getValue().toString();

                final int j = i - 1;


                handler.postDelayed(new Runnable() {
                    public void run() {

                        TV_option_a.setEnabled(true);
                        TV_option_b.setEnabled(true);
                        TV_option_c.setEnabled(true);
                        TV_option_d.setEnabled(true);

                        Animation bottomToTop = AnimationUtils.loadAnimation(SinglePlayerActivity.this, R.anim.anim_alpha);
                        quiz_question.startAnimation(bottomToTop);
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                quiz_question.setText(j + " . " + Question);
                            }
                        }, 1000);

                        TV_option_a.startAnimation(bottomToTop);
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                TV_option_a.setText(option_a);
                            }
                        }, 1000);
                        TV_option_b.startAnimation(bottomToTop);
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                TV_option_b.setText(option_b);
                            }
                        }, 1000);

                        TV_option_c.startAnimation(bottomToTop);
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                TV_option_c.setText(option_c);
                            }
                        }, 1000);

                        TV_option_d.startAnimation(bottomToTop);
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                TV_option_d.setText(option_d);
                            }
                        }, 1000);
                        handler.postDelayed(new Runnable() {
                            public void run() {
                        reverseTimer(option_a, option_b, option_c, option_d);
                            }
                        }, 2500);

                    }
                }, 1500);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        i++;
         }
         }
         }, 2000); }
         }.start(); }
        }, 1000);


    }
    public void reverseTimer(final String op_a, final String op_b, final String op_c, final String op_d){

            new CountDownTimer(15000, 1000) {

                public void onTick(long millisUntilFinished) {

                    if (isPaused || isCanceled) {
                        cancel();
                    } else {

                        MainCounter.setText("" + millisUntilFinished / 1000);
                    }
                }

                public void onFinish() {

                    MainCounter.setText("0");
                    final String mainCounterText = MainCounter.getText().toString();
                    if (mainCounterText.equals("0")) {
                        getResult(op_a, op_b, op_c, op_d);
                    }
                }
            }.start();


    }


    public void getResult(final String op_a, final String op_b, final String op_c, final String op_d){



        handler.postDelayed(new Runnable() {
            public void run() {

                if (User_Answer.equals(answer)) {
                    Score = Score + 10;

                    final Dialog alertDialog = new Dialog(SinglePlayerActivity.this);
                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alertDialog.setContentView(R.layout.dialog_answer);
                    alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    ImageView smiley_answer = alertDialog.findViewById(R.id.smiley_answer);
                    smiley_answer.setImageResource(R.drawable.ic_happy);
                    TextView result_text = alertDialog.findViewById(R.id.result_text);
                    Animation bottomToTop = AnimationUtils.loadAnimation(SinglePlayerActivity.this, R.anim.anim_alpha);
                    result_text.startAnimation(bottomToTop);
                    result_text.setText("CORRECT");
                    result_text.setTextColor(Color.GREEN);

                    handler.postDelayed(new Runnable() {
                        public void run() {
                            alertDialog.dismiss();
                        }
                    }, 2000);

                    alertDialog.show();


                    if (User_Answer.equals(op_a)) {
                        TV_option_a.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                        TV_option_a.setTextColor(Color.WHITE);
                    } else if (User_Answer.equals(op_b)) {
                        TV_option_b.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                        TV_option_b.setTextColor(Color.WHITE);

                    } else if (User_Answer.equals(op_c)) {
                        TV_option_c.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                        TV_option_c.setTextColor(Color.WHITE);

                    } else if (User_Answer.equals(op_d)) {
                        TV_option_d.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                        TV_option_d.setTextColor(Color.WHITE);
                    } else {

                    }
                    String viewScore = String.valueOf(Score);
                    nextQuizCounter.setText(viewScore);
                } else {

                    final Dialog alertDialog = new Dialog(SinglePlayerActivity.this);
                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alertDialog.setContentView(R.layout.dialog_answer);
                    alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    TextView result_text = alertDialog.findViewById(R.id.result_text);
                    ImageView smiley_answer = alertDialog.findViewById(R.id.smiley_answer);
                    smiley_answer.setImageResource(R.drawable.ic_crying);
                    Animation bottomToTop = AnimationUtils.loadAnimation(SinglePlayerActivity.this, R.anim.anim_alpha);
                    result_text.startAnimation(bottomToTop);
                    result_text.setText("Wrong Answer");
                    result_text.setTextColor(Color.RED);

                    handler.postDelayed(new Runnable() {
                        public void run() {
                            alertDialog.dismiss();
                        }
                    }, 2000);

                    alertDialog.show();

                    if (answer.equals(op_a)) {
                        TV_option_a.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                        TV_option_a.setTextColor(Color.WHITE);
                    } else if (answer.equals(op_b)) {
                        TV_option_b.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                        TV_option_b.setTextColor(Color.WHITE);

                    } else if (answer.equals(op_c)) {
                        TV_option_c.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                        TV_option_c.setTextColor(Color.WHITE);

                    } else if (User_Answer.equals(op_d)) {
                        TV_option_d.setBackgroundResource(R.drawable.bg_game_text_view_correct);
                        TV_option_d.setTextColor(Color.WHITE);
                    } else {

                    }

                    if (User_Answer.equals(op_a)) {
                        TV_option_a.setBackgroundResource(R.drawable.bg_game_text_view_incorrect);
                        TV_option_a.setTextColor(Color.WHITE);
                    } else if (User_Answer.equals(op_b)) {
                        TV_option_b.setBackgroundResource(R.drawable.bg_game_text_view_incorrect);
                        TV_option_b.setTextColor(Color.WHITE);

                    } else if (User_Answer.equals(op_c)) {
                        TV_option_c.setBackgroundResource(R.drawable.bg_game_text_view_incorrect);
                        TV_option_c.setTextColor(Color.WHITE);

                    } else if (User_Answer.equals(op_d)) {
                        TV_option_d.setBackgroundResource(R.drawable.bg_game_text_view_incorrect);
                        TV_option_d.setTextColor(Color.WHITE);
                    } else {
                    }
                }
            }
        }, 1000);

        if (i <= 5){

            handler.postDelayed(new Runnable() {
                public void run() {
                    getQuiz();
                }
            }, 2000);

            }
            else{

            handler.postDelayed(new Runnable() {
                public void run() {
                    addDBSingleScore();
                }
            }, 3000);

                }

    }

    public void addDBSingleScore(){
        FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
         String uid=current_user.getUid();

        DatabaseReference firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("SingleUserSB").child(uid);
        // String deviceToken = FirebaseInstanceId.getInstance().getToken();

        HashMap<String,String> userMap=new HashMap<String, String>();

        String randomRecord = firebaseDatabase.push().getKey();

        userMap.put("level","1");
        userMap.put("player",playerName);
        userMap.put("score", String.valueOf(Score));

        firebaseDatabase.child(randomRecord).setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){

                    final Dialog alertDialog = new Dialog(SinglePlayerActivity.this);
                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alertDialog.setContentView(R.layout.dialog_answer);
                    alertDialog.setCancelable(false);
                    alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    ImageView smiley_answer = alertDialog.findViewById(R.id.smiley_answer);
                    smiley_answer.setImageResource(R.drawable.ic_happy);
                    TextView result_text = alertDialog.findViewById(R.id.result_text);
                    Animation bottomToTop = AnimationUtils.loadAnimation(SinglePlayerActivity.this, R.anim.anim_alpha);
                    result_text.startAnimation(bottomToTop);
                    result_text.setText("Your Score :" + Score);
                    result_text.setTextColor(Color.YELLOW);

                    alertDialog.setOnKeyListener(new Dialog.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keyCode,
                                             KeyEvent event) {
                            // TODO Auto-generated method stub
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                alertDialog.dismiss();
                                Intent LevelChooserIntent = new Intent(SinglePlayerActivity.this, LevelChooserActivity.class);
                                LevelChooserIntent.putExtra("key","single");
                                LevelChooserIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(LevelChooserIntent);
                                finish();
                                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

                            }
                            return true;
                        }
                    });
                    alertDialog.show();

                }
            }
        });

    }


    @Override
    public boolean onSupportNavigateUp(){
        isPaused = true;
        Intent i = new Intent(SinglePlayerActivity.this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        return true;
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to go back?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        isPaused = true;
                      //  stopMP();
                        Intent intent = new Intent(SinglePlayerActivity.this, LevelChooserActivity.class);
                        intent.putExtra("key","single");
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.option_a:

                TV_option_a.setBackgroundResource(R.drawable.bg_game_text_view_is_selected);
                TV_option_a.setTextColor(Color.WHITE);

                User_Answer = TV_option_a.getText().toString();

                TV_option_b.setEnabled(false);
                TV_option_c.setEnabled(false);
                TV_option_d.setEnabled(false);


                break;
            case R.id.option_b:

                TV_option_b.setBackgroundResource(R.drawable.bg_game_text_view_is_selected);
                TV_option_b.setTextColor(Color.WHITE);

                User_Answer = TV_option_b.getText().toString();

                TV_option_a.setEnabled(false);
                TV_option_c.setEnabled(false);
                TV_option_d.setEnabled(false);

                break;
            case R.id.option_c:

                TV_option_c.setBackgroundResource(R.drawable.bg_game_text_view_is_selected);
                TV_option_c.setTextColor(Color.WHITE);

                User_Answer = TV_option_c.getText().toString();

                TV_option_b.setEnabled(false);
                TV_option_a.setEnabled(false);
                TV_option_d.setEnabled(false);

                break;
            case R.id.option_d:

                TV_option_d.setBackgroundResource(R.drawable.bg_game_text_view_is_selected);
                TV_option_d.setTextColor(Color.WHITE);

                User_Answer = TV_option_d.getText().toString();

                TV_option_b.setEnabled(false);
                TV_option_c.setEnabled(false);
                TV_option_a.setEnabled(false);
                break;
            case R.id.sb_list_dialog:

                String opi_a = TV_option_a.getText().toString();
                String opi_b = TV_option_b.getText().toString();
                String opi_c = TV_option_c.getText().toString();
                String opi_d = TV_option_d.getText().toString();

                getResult(opi_a,opi_b,opi_c,opi_d);

                break;
        }
    }
}
