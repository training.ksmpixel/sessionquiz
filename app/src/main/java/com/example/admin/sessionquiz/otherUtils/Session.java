package com.example.admin.sessionquiz.otherUtils;


import android.content.Context;
import android.content.SharedPreferences;

public class Session {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;

    String appPref = "Quiz_APP";

    public Session(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences(appPref, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void setLoggedIn(boolean loggedIn){
        editor.putBoolean("LoggedInMode", loggedIn);
        if (loggedIn == false){
            editor.clear();
        }
        editor.commit();
    }

    public boolean loggedIn(){
        return sharedPreferences.getBoolean("LoggedInMode",false);
    }
}
