package com.example.admin.sessionquiz.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.admin.sessionquiz.Animation.AnimationUtil;
import com.example.admin.sessionquiz.PojoClass.onlineUserInvitePojo;
import com.example.admin.sessionquiz.R;
import com.example.admin.sessionquiz.otherUtils.SingleTon;
import com.example.admin.sessionquiz.otherUtils.StaticVariables;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OnlineUserInviteAdapter  extends RecyclerView.Adapter<OnlineUserInviteAdapter.OnlineUserInviteViewHolder> {
        private Context context;
        private List<onlineUserInvitePojo> onlineUserInvitePojoList;
        private int previousPosition = 0;
        int game_id;

    StaticVariables staticVariables = new StaticVariables();
    public String FCM_NOTIFY_API = staticVariables.FCM_NOTIFY_API;
    private RequestQueue Queue;
    String fcm_id;


    public OnlineUserInviteAdapter(Context context, List<onlineUserInvitePojo> onlineUserInvitePojoList,int game_id) {

            this.onlineUserInvitePojoList = onlineUserInvitePojoList;
            this.context=context;
            this.game_id = game_id;

        }

        @NonNull
        @Override
        public OnlineUserInviteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.single_item_game_invite_layout, null);
            view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
            Queue = SingleTon.getInstance(context.getApplicationContext()).getRequestQueue();
            return new OnlineUserInviteViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final OnlineUserInviteViewHolder holder, int position) {
            final onlineUserInvitePojo onlineUserInvitePojo = onlineUserInvitePojoList.get(position);
            FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
            final String current_user_uid = current_user.getUid();

            final String FCM_DEVICE_TOKEN = onlineUserInvitePojo.getDevice_token();

            final String list_user_uid = onlineUserInvitePojo.getUid();

            if(current_user_uid.equals(list_user_uid)){
                holder.user_invite.setVisibility(View.GONE);
            }//else {

                holder.user_name.setText(onlineUserInvitePojo.getUser_name());
                holder.user_level.setText(onlineUserInvitePojo.getLevel());
                holder.user_image.setImageResource(R.drawable.ic_user_profile);

                    holder.user_invite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.user_invite.setEnabled(false);
                        final DatabaseReference firebaseDatabase = FirebaseDatabase.getInstance().getReference("Game_Notification").child(list_user_uid);
                        final String randomRecord = firebaseDatabase.push().getKey();
                        DatabaseReference firebaseDatabase1 = FirebaseDatabase.getInstance().getReference("Game_Notification").child(list_user_uid);

                        String get_id = String.valueOf(game_id);
                       // Toast.makeText(context,"ID "+get_id,Toast.LENGTH_SHORT).show();
                        firebaseDatabase1.orderByChild("from").equalTo(current_user_uid)
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.exists()){
                                           Toast.makeText(context,"Already Sent",Toast.LENGTH_SHORT).show();
                                            holder.user_invite.setText("Already Sent");
                                        } else {

                                            HashMap<String, String> notificationMap = new HashMap<String, String>();
                                            notificationMap.put("from", current_user_uid);
                                            notificationMap.put("to", list_user_uid);
                                            notificationMap.put("game_id", String.valueOf(game_id));
                                            notificationMap.put("type", randomRecord);

                                            firebaseDatabase.child(randomRecord).setValue(notificationMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    holder.user_invite.setText("SENT");
                                                }
                                            });


                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });


                        StringRequest stringRequest = new StringRequest(Request.Method.POST, FCM_NOTIFY_API,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

/*
                                        JSONObject jsonObject = null;
                                        try {
                                            jsonObject = new JSONObject(response);
                                            String JSONResponse = jsonObject.getString("response");
                                            JSONObject jsonObjResponse = new JSONObject(JSONResponse);
                                            String code = jsonObjResponse.getString("code");
                                            final String message = jsonObjResponse.getString("message");
                                            String result = jsonObjResponse.getString("result");
                                            JSONArray jsonArray = new JSONArray(result);



                                        }catch (JSONException e) {
                                            e.printStackTrace();
                                        }*/

                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("TAG", "Error: " + String.valueOf(error.getMessage()));
                                Toast.makeText(context, "Error: " + String.valueOf(error.getMessage()), Toast.LENGTH_SHORT).show();
                            }


                        }){
                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<>();
                                params.put("id",FCM_DEVICE_TOKEN);
                                Log.e("DCM ID ",FCM_DEVICE_TOKEN);
                                return params;
                            }
                        };

                        Queue.add(stringRequest);

                       /* DatabaseReference Game_Creation = FirebaseDatabase.getInstance().getReference("Game_Created").child(current_user_uid).child(current_user_uid);
                        Game_Creation.setValue(current_user_uid);*/

                    }
                });

                if (position > previousPosition) {
                    AnimationUtil.animateUpDown(holder, true);
                } else {
                    AnimationUtil.animateUpDown(holder, false);
                }

                previousPosition = position;

            }
       // }

        @Override
        public int getItemCount() {
            return onlineUserInvitePojoList.size();
        }

        public class OnlineUserInviteViewHolder extends RecyclerView.ViewHolder{

            LinearLayout rootView;
            ImageView user_image;
            TextView user_name,user_level;
            Button user_invite;


            public OnlineUserInviteViewHolder(View itemView) {
                super(itemView);

                rootView = itemView.findViewById(R.id.rootView);
                user_image = itemView.findViewById(R.id.user_image);
                user_name = itemView.findViewById(R.id.user_name);
                user_level = itemView.findViewById(R.id.user_level);
                user_invite = itemView.findViewById(R.id.user_invite);
                user_invite.setText("Invite");



            }
        }
}
