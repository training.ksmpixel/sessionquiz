package com.example.admin.sessionquiz.Adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.sessionquiz.Animation.AnimationUtil;
import com.example.admin.sessionquiz.R;
import com.example.admin.sessionquiz.PojoClass.SingleLevelPojo;
import com.example.admin.sessionquiz.otherUtils.StaticVariables;

import java.util.ArrayList;
import java.util.List;

public class MultiLevelAdapter extends RecyclerView.Adapter<MultiLevelAdapter.MyViewHolder> implements Filterable {

    private Context context;
    private List<SingleLevelPojo> singleLevelPojoList;
    private List<SingleLevelPojo> singleLevelPojoListFiltered;
    private MultiLevelAdapterListener listener;
    private int previousPosition = 0;

    StaticVariables staticVariables = new StaticVariables();

    public MultiLevelAdapter(Context context, List<SingleLevelPojo> singleLevelPojoList, MultiLevelAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.singleLevelPojoList = singleLevelPojoList;
        this.singleLevelPojoListFiltered = singleLevelPojoList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_level_indicator, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        final SingleLevelPojo singleLevelPojo = singleLevelPojoListFiltered.get(position);
        holder.Level.setText(singleLevelPojo.getLevel_id());

        String levelUnlocked = singleLevelPojo.getLocked();
        if(levelUnlocked.equals("locked")){
            holder.locked_imag.setImageResource(R.drawable.ic_level_locked);
        }else{
            holder.locked_imag.setImageResource(R.drawable.ic_level_unlocked);
        }


        if(position > previousPosition) {
            // We are scrolling DOWN
            AnimationUtil.animateUpDown(holder, true);
        } else {
            // We are scrolling UP
            AnimationUtil.animateUpDown(holder, false);
        }

        previousPosition = position;


    }

    @Override
    public int getItemCount() {
        return singleLevelPojoListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    singleLevelPojoListFiltered = singleLevelPojoList;
                } else {
                    List<SingleLevelPojo> filteredList = new ArrayList<>();
                    for (SingleLevelPojo row : singleLevelPojoList) {

                    }

                    singleLevelPojoListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = singleLevelPojoListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                singleLevelPojoListFiltered = (ArrayList<SingleLevelPojo>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface MultiLevelAdapterListener {
        void onLevelItemSelected(SingleLevelPojo singleLevelPojo);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView Level;
        ImageView locked_imag;


        public MyViewHolder(View itemView) {
            super(itemView);

            Level = itemView.findViewById(R.id.level_count);
            locked_imag = itemView.findViewById(R.id.locked_level);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    listener.onLevelItemSelected(singleLevelPojoListFiltered.get(getAdapterPosition()));
                }
            });

        }
    }

}
