package com.example.admin.sessionquiz.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.admin.sessionquiz.Adapter.OnlineUserInviteAdapter;
import com.example.admin.sessionquiz.PojoClass.onlineUserInvitePojo;
import com.example.admin.sessionquiz.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;

public class MultiPlayerGameActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<onlineUserInvitePojo> onlineUserInvitePojoList ;
    private OnlineUserInviteAdapter onlineUserInviteAdapter;

    ImageView game_req;
    TextView timerCount;
    Button user_invite_done;

    ProgressBar progressBar;
    int n;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_player_game);

        Random rand = new Random();
        n = rand.nextInt(5) + 1;

        progressBar = findViewById(R.id.progressBar);

        recyclerView = findViewById(R.id.mul_recycler_view);
        timerCount = findViewById(R.id.timer_count);

        game_req = findViewById(R.id.game_req);
        onlineUserInvitePojoList = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);// new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);
        onlineUserInviteAdapter =  new OnlineUserInviteAdapter(this, onlineUserInvitePojoList,1);

        recyclerView.setAdapter(onlineUserInviteAdapter);

        final DatabaseReference FirebaseTimer = FirebaseDatabase.getInstance().getReference("Timer").child("game_Id");
       FirebaseTimer.addValueEventListener(new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               String Timer = dataSnapshot.getValue().toString();
               timerCount.setText(Timer);


           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }
       });

        game_req.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MultiPlayerGameActivity.this, NotificationActivity.class);
                startActivity(intent);
            }
        });

        FetchLevelList();
    }

    public void FetchLevelList(){

        FirebaseUser current_player = FirebaseAuth.getInstance().getCurrentUser();
        String player_uid=current_player.getUid();
        final DatabaseReference GamePlayerDB= FirebaseDatabase.getInstance().getReference("Users");
        onlineUserInvitePojoList.clear();
        GamePlayerDB.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                onlineUserInvitePojo onlineUserInvitePojo = dataSnapshot.getValue(onlineUserInvitePojo.class);
                onlineUserInvitePojoList.add(onlineUserInvitePojo);
                onlineUserInviteAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                FetchLevelList();
                onlineUserInviteAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                FetchLevelList();
                onlineUserInviteAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
    @Override
    public void onBackPressed() {
        Intent LevelChooserIntent = new Intent(MultiPlayerGameActivity.this, MainActivity.class);
        LevelChooserIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        LevelChooserIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(LevelChooserIntent);
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

}
