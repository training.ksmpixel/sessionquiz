package com.example.admin.sessionquiz.PojoClass;

public class MultiScorePojo {

    String name;
    int score;
    String uid;

    public MultiScorePojo() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public MultiScorePojo(String name, int score, String uid) {
        this.name = name;
        this.score = score;
        this.uid = uid;
    }
}
