package com.example.admin.sessionquiz.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.sessionquiz.Activity.MultiLevelGameActivity;
import com.example.admin.sessionquiz.Activity.MultiPlayerActivity;
import com.example.admin.sessionquiz.PojoClass.GameReqPojo;
import com.example.admin.sessionquiz.R;
import com.example.admin.sessionquiz.otherUtils.StaticVariables;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Mul_Game_Req_Adapter extends RecyclerView.Adapter<Mul_Game_Req_Adapter.Game_Req_ViewHolder> {

    List<GameReqPojo> gameReqPojoList = new ArrayList<>();
    Context context;
    DatabaseReference gameReqDB;
    int game_id;
    StaticVariables staticVariables = new StaticVariables();
    SharedPreferences sharedPreferences;
    String player_Name;
    FirebaseUser current_user;
    String current_user_uid;

    //database reference
    DatabaseReference NotifyAccepted;
    DatabaseReference Game_CreationUsers;
    DatabaseReference Game_creation;

    //value event listener
    ValueEventListener GameReqListener;
    ValueEventListener GameCreationListener;



    public Mul_Game_Req_Adapter(Context context,List<GameReqPojo> gameReqPojoList,int game_id ) {
        this.gameReqPojoList = gameReqPojoList;
        this.context = context;
        this.game_id = game_id;
    }

    @NonNull
    @Override
    public Game_Req_ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.game_req_list_single_item, parent, false);
        itemView.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        sharedPreferences = context.getSharedPreferences(staticVariables.LOG_PREF,Context.MODE_PRIVATE);
        player_Name = sharedPreferences.getString(staticVariables.PLAYER_NAME,"NULL");
        return new Game_Req_ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final Game_Req_ViewHolder holder, final int position) {

        GameReqPojo gameReqPojo = gameReqPojoList.get(position);
        final String from = gameReqPojo.getFrom();
        final String to = gameReqPojo.getTo();
        final String game_id = gameReqPojo.getGame_id();
        final String refnode_key =gameReqPojo.getType();
        current_user = FirebaseAuth.getInstance().getCurrentUser();
        current_user_uid = current_user.getUid();

        gameReqDB = FirebaseDatabase.getInstance().getReference().child("Users").child(from);
        gameReqDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final String reqPlayer = dataSnapshot.child("user_name").getValue().toString();

                holder.invitedUserName.setText(reqPlayer);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        holder.AcceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.AcceptButton.setText("Invite Accepted");
                holder.AcceptButton.setEnabled(false);
                holder.DeclineButton.setVisibility(View.GONE);


                //database reference
                NotifyAccepted = FirebaseDatabase.getInstance().getReference("Notification_Accepted").child(current_user_uid);
                Game_CreationUsers = FirebaseDatabase.getInstance().getReference("Game_Accepted").child(from).child("gamePlayers").child(current_user_uid);
                Game_creation = FirebaseDatabase.getInstance().getReference("Game_Created");

               GameCreationListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.child(from).exists()){

                            String game_Timer = dataSnapshot.child(from).child("game_timer").getValue().toString();

                            NotifyAccepted.child("creator_id").setValue(from);
                            NotifyAccepted.child("game_id").setValue(game_id);
                            NotifyAccepted.child("game_timer").setValue(game_Timer);

                            Game_CreationUsers.child("name").setValue(player_Name);
                            Game_CreationUsers.child("uid").setValue(current_user_uid);
                            Game_CreationUsers.child("score").setValue(0);

                            }else {
                            Game_creation.removeEventListener(GameCreationListener);

                           // Toast.makeText(context,"no records found",Toast.LENGTH_SHORT).show();
                            DatabaseReference fdbNotifyRemov = FirebaseDatabase.getInstance().getReference("Game_Notification").child(current_user_uid);
                            fdbNotifyRemov.child(refnode_key).removeValue();
                            }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                        };
               Game_creation.addValueEventListener(GameCreationListener);
                }
        });
        holder.DeclineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DatabaseReference firebaseDatabase1 = FirebaseDatabase.getInstance().getReference("Game_Notification").child(current_user_uid);
                firebaseDatabase1.child(refnode_key).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        firebaseDatabase1.child(refnode_key).removeValue();
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                }
        });
        }
        @Override
    public int getItemCount() {
        return gameReqPojoList.size();
    }
    public class Game_Req_ViewHolder extends RecyclerView.ViewHolder{

        TextView invitedUserName;
        Button AcceptButton;
        Button DeclineButton;

        public Game_Req_ViewHolder(View itemView) {
            super(itemView);
            invitedUserName = itemView.findViewById(R.id.tc_invite_from);
            AcceptButton = itemView.findViewById(R.id.accept_btn);
            DeclineButton = itemView.findViewById(R.id.decline_btn);
            }
            }
    }


